
#!/usr/bin/env bash

clear

if [[ -z $1 ]] || [[ -z $2 ]]; then
       echo "usage: ./install.sh <next_ip> <next_pass>"
        exit 1
fi

next_ip=$1
next_pass=$2

wget https://git.io/vpn -O openvpn-install.sh #&& bash openvpn-install.sh
sed -r 's/read\ -p\ \"Client\ name:\ \"\ -e\ -i\ client\ CLIENT/CLIENT="next"/g' openvpn-install.sh  > openvpn-install-next.sh

bash openvpn-install-next.sh

mkdir /root/ovpn
mv /root/next.ovpn /root/ovpn/next.ovpn
mv openvpn-install.sh /root/ovpn/openvpn-install.sh

echo "#!/usr/bin/env bash" > /root/ovpn/start.sh
echo "openvpn --config /root/ovpn/next.ovpn &" >> /root/ovpn/start.sh
cd /root
tar -cvzf ovpn.tar.gz ovpn/*

apt-get install -y sshpass

sshpass -p$next_pass scp /root/ovpn.tar.gz root@${next_ip}:/root/ovpn.tar.gz
cat /root/ovpn.tar.gz | sshpass -p "$next_pass" ssh root@${next_ip} "tar xzf -"
#sshpass -p "$next_pass" ssh root@${next_ip} 'tar -xvf ~/ovpn.tar.gz'

sshpass -p "$next_pass" ssh root@${next_ip}
		
